<?php 
	function get_space($pool) {
                $pool_total_space = disk_total_space($pool);
                $pool_free_space = disk_free_space($pool);

                $pool_space =  array(
                        'total_space'   => $pool_total_space,
                        'free_space'    => $pool_free_space,
                        'used_space'    => $pool_total_space - $pool_free_space,
                        'pool'          => $pool,
                        'size_type'     => SIZE_TYPE,
                );

		return $pool_space;
        }
?>

<?php
	define("SIZE_TYPE",     "bytes");

	$pool = strip_tags($_GET['pool']);
	$pools = strip_tags($_GET['pools']);

	$pool_sizes = array();
	$json = array();

	if (isset($pool) && !empty($pool)) {
		array_push($pool_sizes, get_space($pool));
	}

	else if (isset($pools) && !empty($pools)) {
		$temp_pools = explode(',',$pools);
		
		foreach ($temp_pools as $pool) {
			array_push($pool_sizes, get_space($pool));
		}
	}

	else {
		$json['error'] = 'You must specify either pool or pools.';
	}
	
	$json['pools'] = $pool_sizes;
	$json['server_time'] = time();
	echo json_encode($json);
?>
